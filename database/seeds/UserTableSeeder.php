<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Adviser;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $u1=new User();
        $u1->name='admin';
        $u1->password=bcrypt('123456');
        $u1->type=1;
        $u1->save();
        
        $u1=new User();
        $u1->name='doctor';
        $u1->password=bcrypt('123456');
        $u1->type=2;
        $u1->save();
        
        $u1=new User();
        $u1->name='coach';
        $u1->password=bcrypt('123456');
        $u1->type=3;
        $u1->save();
        
        $u1=new Adviser();
        $u1->name='doctor';
        $u1->type=2;
        $u1->countUsers=0;
        $u1->save();
        
        $u1=new Adviser();
        $u1->name='coach';
        $u1->type=3;
        $u1->countUsers=0;
        $u1->save();
    }
}
