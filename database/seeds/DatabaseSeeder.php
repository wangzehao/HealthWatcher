<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(ActivityTableSeeder::class);
        $this->call(HealthDataSeeder::class);
        $this->call(InterestGroupSeeder::class);
        $this->call(FriendsSeeder::class);
        $this->call(adviceRuleSeeder::class);
        Model::reguard();
    }
}
