<?php

use Illuminate\Database\Seeder;
use App\AdviceRule;

class adviceRuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        	$po=new AdviceRule();
        	$po->title="加钱运动的建议";
        	$po->describe="对7天内运动时间小于20分钟天数大于等于5天，或者总运动时间不足60分钟的人进行建议";
        	$po->rule='$rule->activeTime=7;
				$rule->dateType="血压";
				$rule->bottom=20;
				$rule->assert="return true;";';
        	$po->content="该运动了，少年";
        	$po->save();
        //"$belowTime>=5||$sum<=60"
    }
}
