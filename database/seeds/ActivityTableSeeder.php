<?php

use Illuminate\Database\Seeder;
class ActivityTableSeeder extends Seeder
{
    public function run()
    {
        factory('App\Activity', 20)->create();
    }

}