<?php

use Illuminate\Database\Seeder;
use App\Group;
use App\Http\Controllers\UserController;
use App\Post;

class InterestGroupSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for($i=0;$i<20;$i++){
			$newGroup = new Group;
			$newGroup->name = "group".$i;
			$newGroup->save();
		}
		for($i=0;$i<40;$i++){
			$post=new Post();
			$post->title="this is post".$i."'s title.";
			$post->content="this is a post's content.this is post".$i."'s content.this is a post's content.this is a post's content.this is a post's content.this is a post's content.this is a post's content.";
			$post->publisher=1;
			$post->group='group0';
			$post->father='0';
			$post->save();
		}
		for($i=0;$i<40;$i++){
			$post=new Post();
			$post->title="this is post".$i."'s title.";
			$post->content="this is a post's content.this is post".$i."'s content.this is a post's content.this is a post's content.this is a post's content.this is a post's content.this is a post's content.";
			$post->publisher=1;
			$post->group='group0';
			$post->father='1';
			$post->save();
		}
    }
}
