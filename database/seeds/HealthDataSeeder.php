<?php

/**
 * @author:ivan
 * Time: 2015/12/4 10:51
 */
use Illuminate\Database\Seeder;
use Carbon\Carbon;
use \App\Health;
use \App\Sports;
class HealthDataSeeder extends Seeder
{
    public function run()
    {
        factory('App\Health', 70)->create();
        $start=Carbon::createFromDate(2015,10,4);
        $all=Health::all();
        for($i=0;$i<count($all);$i++)
        {
            $all[$i]->update(["date"=>$start->toDateString()]);
            $start=$start->addDay(1);
        }

        factory('App\Sports', 70)->create();
        $start=Carbon::createFromDate(2015,10,4);
        $all=Sports::all();
        for($i=0;$i<count($all);$i++)
        {
            $all[$i]->update(["date"=>$start->toDateString()]);
            $start=$start->addDay(1);
        }
    }
}