<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Health::class, function (Faker\Generator $faker) {
    return [
        'user' => 'ivan',
        'height'=>rand(170,172),
        'weight'=>rand(60,70),
        'blood_pressure'=>rand(120,130)."/".rand(90,100),
        'heart_rate'=>rand(60,70),
        'date'=>"2015-12-04",

    ];
});


$factory->define(App\Sports::class, function (Faker\Generator $faker) {
    return [
        'user' => 'ivan',
        'distance'=>rand(0,10),
        'time'=>rand(0,120),
        'heat'=>rand(0,20),
        'steps'=>rand(0,1000),
        'date'=>"2015-12-04",
    ];
});


$factory->define(App\Activity::class, function (Faker\Generator $faker) {
    return [
        'title'=>'周六晚金鸡湖夜跑！',
        'description'=>'夜晚的相门城门华灯初上，护城河上不时吹来习习凉风，体感舒适，夜景也非常绚丽。城门两边的步道比较平整，非常适合跑步或快走。相门城墙晚上也是可以上去的 ，爬上城楼同样是一种不错锻炼。附近有老人跳广场舞、舞剑，还有家长带着孩子出来散步的，所以夜跑的沿途都很热闹，即使是独自一人也不会觉得孤独。

桐泾公园 桐泾公园位于桐泾路与解放路交界处东北角，占地约18万平方米，是苏州市中心城区最大的市级公园。无论是夜跑还是散步都很合适。

桂花公园 桂花公园在竹辉桥附近。两公里长的婉延曲径沿运河穿梭在茂密的柳荫绿岛树林中，草坪宽广，自然舒适，是市民跑步、散步、观景的好地方。

苏州公园 就是大家熟悉的大公园，晚上也开放到8点多。苏州公园虽经多次整修，仍保持了建园初期形成的法国式花园与中国自然山水交融的布局风格。2001年全面改造后，环境更好，设施功能也更健全，是周边居民休闲、跑步的好去处。

环金鸡湖 环金鸡湖夜跑是相当休闲的一条路线，全程15KM左右，虽然全程较长，但由于金鸡湖环线景色优美，景点众多，空气也不错，跑跑就心情愉悦，跑累了还可以在路边休息一下，看看风景，谈笑风生中不知不觉跑完全程。'
    ];
});