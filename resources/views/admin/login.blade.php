<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>HealthWatcher</title>

    <!-- Bootstrap -->
    <link href="../common/css/bootstrap.min.css" rel="stylesheet">

    <!--footer.css-->
    <link href="../common/css/footer.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        h1 {font-size: 3.5em;margin-top:6%;margin-bottom: 4%}
        .login-form{padding-left: 3%;padding-right:3%}
        .group-gap{margin-bottom: 8%;}
    </style>
</head>
<body>
<div class="container">
    <div class="row-fluid">
        <div class="col-md-12">
            <h1 class="text-center text-primary">
                HealthWatcher
            </h1>
        </div>
    </div>
    <div class="row-fluid">
        <div class="col-xs-2 col-md-4"></div>
        <div class="col-xs-8 col-md-4  login-form">
            <form method="post" action="/admin">
                {!! csrf_field() !!}
                <div class="form-group group-gap">
                    <input type="text" class="form-control input-lg" name="name" id="exampleInputEmail1" placeholder="username" >
                </div>
                <div class="form-group group-gap ">
                    <input type="password" class="form-control input-lg" name="password" id="exampleInputPassword1" placeholder="Password" >
                </div>
                <div class="form-group group-gap ">
                <button type="submit" class="btn btn-block btn-primary btn-lg btn-login">Login</button>
                </div>
                @if($errors->any())
                    <ul class="alert alert-danger" style="list-style-type:none">
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                    </ul>
                @endif
            </form>
        </div>
        <div class="col-xs-2 col-md-4"></div>
    </div>
</div>

<footer class="footer">

</footer>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="//cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../common/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function(){
        $("footer").load("../common/footer.html");
    });
</script>
</body>
</html>