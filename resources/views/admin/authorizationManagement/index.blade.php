@extends('adminApp')
@section('otherResource')
    <style>
        form{
            margin-top:3em;
        }
        .title{
            font-size: 1.6em;
        }
        .form-group{
            margin-bottom: 4em;
        }

        .role{
            font-size:1.5em;
        }

    </style>

@endsection


@section('rightPanel')
    <div class="row">
        <h1 class="page-header col-md-11">分配权限</h1>
    </div>

    <form method='post' class="form-horizontal" action='/admin/authorization'>
        {!! csrf_field() !!}
        <div class="form-group">
            <label for="title" class="col-md-2 col-sm-2 col-lg-1  title">用户 </label>
            <div class="col-md-4 col-sm-6">
                <input type="text" class="form-control input-lg" name="name" id="title" placeholder="用户名">
            </div>
        </div>

        <div class="form-group">
            <label for="start" class="col-md-2 col-sm-2 col-lg-1 title ">密码</label>
            <div class="col-md-4 col-sm-6">
                <input type="text" class="form-control input-lg" name="password" id="start" placeholder="密码">
            </div>
        </div>


        <div class="form-group">
            <label for="start" class="col-md-2 col-sm-2 col-lg-1 title ">权限</label>
            <div class="col-md-4 col-sm-6 role">
                <label class="radio-inline">
                    <input type="radio" name="type" id="inlineRadio1" value="3" > 教练
                </label>
                <label class="radio-inline">
                    <input type="radio" name="type" id="inlineRadio2" value="2"> 医生
                </label>
                <label class="radio-inline">
                    <input type="radio" name="type" id="inlineRadio3" value="1"> 管理员
                </label>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-3 col-sm-3 col-lg-2">
                <button type="submit" class="btn btn-default btn-lg  btn-block">确定</button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-4 ">
                @if($errors->any())
                    <ul class="alert alert-danger" style="list-style-type:none">
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                @endif
                @if(\Illuminate\Support\Facades\Session::has('successMsg'))
                        <ul class="alert alert-success" style="list-style-type:none">
                            <li>{{\Illuminate\Support\Facades\Session::get('successMsg')}}</li>
                        </ul>
                @endif
            </div>
        </div>
    </form>
@endsection

@section('otherJs')


@endsection