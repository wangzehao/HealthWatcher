@extends('adviser')

@section('otherResouviserrce')

@endsection
<style>
    .icon{
        margin-left:5px;
    }

</style>

@section('rightPanel')
    <div class="row">
        <h1 class="page-header col-md-11">建议管理</h1>
    </div>
    <h3>所有用户</h3>
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>序号</th>
                <th>待建议用户</th>
                <th>详细健康数据</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $u)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$u['user']}}</td>
                    <td>
                        <span class="glyphicon glyphicon-menu-down icon" aria-hidden="true" role="button" data-toggle="tooltip" data-placement="bottom" title="健康数据"  onclick="document.location='/adviceHome/userHealthData/{{$u['user']}}';"></span>
                        <span class="glyphicon glyphicon-menu-up icon" aria-hidden="true" role="button" data-toggle="tooltip" data-placement="bottom" title="运动数据"  onclick="document.location='/adviceHome/userSportsData/{{$u['user']}}';"></span>
                        <span class="glyphicon glyphicon-pencil icon" aria-hidden="true" role="button" data-toggle="tooltip" data-placement="bottom" title="写建议"  onclick="document.location='/adviceHome/adviceTo/{{$u['user']}}';"></span>

                    </td>
                </tr>
            @endforeach


            </tbody>
        </table>
    </div>
    <div class="row col-md-9">
        {!!$users->render()!!}
    </div>
@endsection

@section('otherJs')
    <script>
        $(function () { $("[data-toggle='tooltip']").tooltip(); });
    </script>

@endsection