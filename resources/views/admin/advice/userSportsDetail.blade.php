@extends('adviser')

@section('otherResouviserrce')

@endsection


@section('rightPanel')
    <div class="row">
        <h1 class="page-header col-md-11">运动数据</h1>
    </div>
    <div class="table-responsive col-md-10">
        <table class="table table-striped table-hover ">
            <thead>
            <tr>
                <th>时间</th>
                <th>运动距离(km)</th>
                <th>运动时间(min)</th>
                <th>燃烧热量(kCal)</th>
                <th>运动步数</th>
            </tr>
            </thead>
            <tbody>
            @foreach($healthData as $u)
                <tr>
                    <td>{{$u['date']}}</td>
                    <td>{{$u['distance']}}</td>
                    <td>{{$u['time']}}</td>
                    <td>{{$u['heat']}}</td>
                    <td>{{$u['steps']}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="row col-md-12">
        <div class="col-lg-6">
            {!!$healthData->render()!!}
        </div>
    </div>


@endsection

@section('otherJs')


@endsection