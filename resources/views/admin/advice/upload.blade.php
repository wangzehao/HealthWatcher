@extends('adviser')

@section('otherResouviserrce')

@endsection


@section('rightPanel')
    <div class="row">
        <h1 class="page-header col-md-11">文件上传</h1>
    </div>

    <form method="POST" action="/adviceHome/upload" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <input type="file" name="myfile" />

        <input type="submit" name="submit" value="Submit" />

    </form>
    @if($errors->any())
        <ul class="alert alert-danger" style="list-style-type:none">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    @endif
    @if(\Illuminate\Support\Facades\Session::has('successMsg'))
        <ul class="alert alert-success" style="list-style-type:none">
            <li>{{\Illuminate\Support\Facades\Session::get('successMsg')}}</li>
        </ul>
    @endif


@endsection

@section('otherJs')

@endsection