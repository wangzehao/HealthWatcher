@extends('adviser')

@section('otherResource')

    <style>
        form{
            margin-top:3em;
        }
        .title{
            font-size: 1.6em;
        }
        .form-group{
            margin-bottom: 3em;
        }

    </style>

@endsection


@section('rightPanel')
    <div class="row">
        <h1 class="page-header col-md-11">写建议</h1>
    </div>

    <form  method="post" action='{{$_SERVER['REQUEST_URI']}}' class="form-horizontal">
        {!! csrf_field() !!}


        <div class="form-group">
            <label for="description" class="col-md-2 col-lg-1 text-center title">建议</label>
            <div class="col-md-12 col-lg-6">
                <textarea class="form-control" rows="15" id="description" name="advice" placeholder="具体内容" ></textarea>
            </div>
            <div class="col-md-4 col-lg-5"></div>
        </div>

        <div class="form-group">
            <div class="col-md-2 ">
                <button type="submit" class="btn btn-default btn-lg  btn-block">确定</button>
            </div>
            <!--
            <div class="col-md-2 ">
                <button type="button" class="btn btn-default btn-lg  btn-block">使用已有建议</button>
            </div>
            -->
        </div>

        <div class="form-group  showMsg">
            <div class="col-md-4 ">
                @if($errors->any())
                    <ul class="alert alert-danger" style="list-style-type:none">
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                @endif
                @if(\Illuminate\Support\Facades\Session::has('successMsg'))
                    <ul class="alert alert-success" style="list-style-type:none">
                        <li>{{\Illuminate\Support\Facades\Session::get('successMsg')}}</li>
                    </ul>
                @endif
            </div>
        </div>

    </form>


@endsection

@section('otherJs')

@endsection

