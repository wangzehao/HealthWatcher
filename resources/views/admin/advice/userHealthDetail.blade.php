@extends('adviser')

@section('otherResouviserrce')

@endsection


@section('rightPanel')
    <div class="row">
        <h1 class="page-header col-md-11">健康数据</h1>
    </div>
    <div class="table-responsive col-md-10">
        <table class="table table-striped table-hover ">
            <thead>
            <tr>
                <th>时间</th>
                <th>身高</th>
                <th>体重</th>
                <th>血压</th>
                <th>心率</th>
            </tr>
            </thead>
            <tbody>
            @foreach($healthData as $u)
                <tr>
                    <td>{{$u['date']}}</td>
                    <td>{{$u['height']}}</td>
                    <td>{{$u['weight']}}</td>
                    <td>{{$u['blood_pressure']}}</td>
                    <td>{{$u['heart_rate']}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="row col-md-12">
        <div class="col-lg-6">
            {!!$healthData->render()!!}
        </div>
    </div>


@endsection

@section('otherJs')


@endsection