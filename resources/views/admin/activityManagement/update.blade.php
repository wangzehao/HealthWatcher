@extends('adminApp')

@section('otherResource')

    <style>
        form{
            margin-top:3em;
        }
        .title{
            font-size: 1.6em;
        }
        .form-group{
            margin-bottom: 3em;
        }

    </style>

@endsection


@section('rightPanel')
    <div class="row">
        <h1 class="page-header col-md-11">修改活动</h1>
    </div>

    <form  method="post" action='/admin/activityManagement/update/{{$activity['id']}}' class="form-horizontal">
        {!! csrf_field() !!}
        <div class="form-group">
            <label for="title" class="col-md-1   title">标题</label>
            <div class="col-md-7">
                <input type="text" class="form-control input-lg" name="title" id="title" value="{{$activity['title']}}">
            </div>
            <div class="col-md-4"></div>
        </div>

        <div class="form-group">
            <label for="description" class="col-md-1 text-center title">描述</label>
            <div class="col-md-7">
                <textarea class="form-control" rows="15" id="description" name="description" >{{$activity['description']}}</textarea>
            </div>
            <div class="col-md-4"></div>
        </div>

        <div class="form-group">
            <div class="col-md-2 ">
                <button type="submit" class="btn btn-default btn-lg  btn-block">保存</button>
            </div>
        </div>
        <div class="form-group  showMsg">
            <div class="col-md-4 ">
                @if($errors->any())
                    <ul class="alert alert-danger" style="list-style-type:none">
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                @endif
                @if(\Illuminate\Support\Facades\Session::has('successMsg'))
                    <ul class="alert alert-success" style="list-style-type:none">
                        <li>{{\Illuminate\Support\Facades\Session::get('successMsg')}}</li>
                    </ul>
                @endif
            </div>
        </div>

    </form>


@endsection

@section('otherJs')

@endsection

