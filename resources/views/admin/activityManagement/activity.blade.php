@extends('adminApp')

@section('otherResource')
        <!--sweet-alert-->
<link rel="stylesheet" type="text/css" href="{{asset('/common/lib/sweet-alert/sweet-alert.css')}}">

<style>
    .delete{
        margin-right: 5px;;
    }
    .add{
        font-size: 1.5em;
    }
</style>
<script src="{{asset('/common/lib/sweet-alert/sweet-alert.js')}}"></script>
@endsection


@section('rightPanel')
    <div class="row">
        <h1 class="page-header col-md-11">活动管理</h1>
        <span class="glyphicon glyphicon-plus   add" aria-hidden="true" role="button"   data-toggle="tooltip" data-placement="bottom" title="新建活动" onclick="document.location='/admin/activityManagement/create';"></span>
    </div>
    <h3>正在进行</h3>
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>序号</th>
                <th>活动名称</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            
            @foreach($activities as $activity)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$activity['title']}}</td>
                    <td><span class="glyphicon glyphicon-minus delete" aria-hidden="true" role="button" data-toggle="tooltip" data-placement="bottom" title="关闭活动"  onclick="
                swal({    title: '',    text: '你确定要删除这个活动吗？',
                        allowOutsideClick:true,   showCancelButton: true,
                        confirmButtonColor: '#DD6B55',    confirmButtonText: '确认',
                        cancelButtonText: '取消',    closeOnConfirm: false  },
                        function(){  location.href='/admin/activityManagement/delete/{{$activity['id']}}';  });
                                "></span>
                        <span class="glyphicon glyphicon-pencil modify" aria-hidden="true" role="button" data-toggle="tooltip" data-placement="bottom" title="修改活动" onclick="document.location='/admin/activityManagement/update/{{$activity['id']}}';"></span>
                    </td>
                </tr>
                @endforeach


            </tbody>
        </table>
    </div>
    <div class="row col-md-9">
        {!!$activities->render()!!}
    </div>
@endsection

@section('otherJs')
    <script>
        /*
        $(document).ready(function(){
            $('.delete').on('click', function(){
                swal({    title: "",    text: "此活动正在进行中，你确定要关闭它吗？",
                            allowOutsideClick:true,   showCancelButton: true,
                            confirmButtonColor: "#DD6B55",    confirmButtonText: "确认",
                            cancelButtonText: "取消",    closeOnConfirm: false  },
                        function(){  location.href='/activity/delete/{{$activity['id']}}';  });
            });
        });
*/
        //tooltip
        $(function () { $("[data-toggle='tooltip']").tooltip(); });
    </script>

@endsection