<?php ?>
@extends('adminApp')

@section('otherResource')
@endsection


@section('rightPanel')
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			
			<div class="row-fluid">
			<h2>
			自动化建议管理 
			
			<a href="#publish" class="btn btn-primary">
				增加建议
			 </a> 
			 <a href="#help" class="btn btn-info">
				规则手册
			 </a>
			 </h2>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<table class="table">
				<thead>
					<tr>
						<th>
						</th>
					</tr>
				</thead>
				<tbody>
				<?php 
					$colorCounter=0;
					foreach($rules as $r){
						echo "<tr";
						if($colorCounter%5==1){
							echo ' class="success"';
						}else if($colorCounter%5==2){
							echo ' class="danger"';
						}else if($colorCounter%5==3){
							echo ' class="warning"';
						}else if($colorCounter%5==4){
							echo ' class="info"';
						}
						echo ">";
						echo "<td>";
						echo "<h3>".$r->title." ";
						echo " <a class='btn btn-danger' href='/autoAdvice/delete?deleteRuleId=".$r->id."'>删除</a>";
						echo "</h3>";
						echo "<h4>".$r->describe."</h4>";
						echo "<h4>规则：".$r->rule."</h4><Br><p>";
						echo "建议内容：".$r->content."<br>";
						echo "<br>创建时间：".$r->created_at."<br>";
						echo "更新时间：".$r->updated_at."<br></p>";
						echo "</tr>";
						
						$colorCounter++;
					}
				
				?>
				
				</tbody>
			</table>
	</div>
	<div class="row-fluid">
		<form name="publish" action="/autoAdvice/add">
			<input id="reply" name="title" type="text" class="form-control" placeholder="建议标题"></input><br>
			<textarea name="describe" class="form-control" placeholder="描述" rows="5"></textarea><br>
			<textarea name="rule" class="form-control" placeholder="规则" rows="10"></textarea><br>
			<textarea name="content" class="form-control" placeholder="建议内容" rows="10"></textarea><br>
			
			<button type="submit" class="btn btn-primary" >发布新建议</button><Br>
		</form>
	</div>
	<div class="row-fluid">
		<form name="help"><h1>规则手册</h1><p>
		$rule为规则对象<br>
		$rule具有的属性有：<br>
		提供的属性：<br>
		$data：数据数组，data[0]表示今天的数据，data[x]表示x天前的数据，可能不存在，不存在值为null<br>
		$avg：表示数据的平均值<br>
		$max：表示数据的最大值<br>
		$min：表示数据的最小值<br>
		$sum：表示数据的和<Br>
		$overTime：表示超过建议最大值的天数<br>
		$belowTime：表示低于建议最小值的天数<br>
		需要设置的属性：<br>
		$activeTime：生效的时间，后面加一个正整数x，表示对近x天内的数据进行分析<br>
		$dateType：数据类型，后面加一个字符串，表示对用户的哪项数据进行分析，可使用“血压”、“心率”、“运动时间”<br>
		$top：表示建议最大值<br>
		$bottom：表示建议最小值<br>
		$assert：断言，断言为字符串表达式，如果该表达式为真，则向用户发出建议<Br>
		</p>
		<h3>使用示例</h3>
		<p>
		$rule->activeTime=7;<br>
		$rule->dateType="运动时间";<br>
		$rule->bottom=20;<br>
		$rule->assert="return $belowTime>=5||$sum<=60;";<br>
		设置为上面的形式则系统会对7天内运动时间小于20分钟天数大于等于5天，或者总运动时间不足60分钟的人发起自动建议。
		</p>
		</form>
	</div>
</div>
@endsection

@section('otherJs')
@endsection