<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>HealthWatcher</title>

    <!-- Bootstrap -->
    <link href="{{asset('/common/css/bootstrap.min.css')}}" rel="stylesheet">

    <!--sidebar-->
    <link href="{{asset('/common/css/sidebar.css')}}" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    @yield('otherResource')

</head>
<body>
<div class="container-fluid">
    <div class="row">
        <!--左面板-->
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="active"><a href="#">{!! $name !!},你好<span class="sr-only">(current)</span></a></li>
                <li><a href="/adviceHome">首页</a></li>
                <!--
                <li><a href="/adviceHome/upload">上传建议</a></li>
                -->
                <li><a href="/logout">登出</a></li>
            </ul>
        </div>

        <!--右面板-->
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 rightPanel">
            @yield('rightPanel')
        </div>

    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset("common/js/jquery-1.11.3.min.js")}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('/common/js/bootstrap.min.js')}}"></script>
@yield('otherJs')
</body>
</html>