<?php
use App\Http\Controllers\AutoAdviceController;
?>
@extends('app')

@section('otherResource')
    <style>
        a:hover {text-decoration: none;}
    </style>
@endsection


@section('rightPanel')
    <div class="row">
        <h1 class="page-header">建议查看</h1>
    </div>

    @foreach($advice as $a)
        <div class="row col-md-9">
            <p>{{$a['adviser'].":".nl2br($a['content'])}}</p>
            <hr/>
        </div>
        
    @endforeach
    <?php $aa=new AutoAdviceController();
    	$as=$aa->getAdvicesForMe();
    	foreach($as as $a)
    	{
			echo "<div class=\"row col-md-9\"><p>";
    		echo "自动建议：".$a." </p>";
			echo "<hr /> </div>";
    	}
    ?>
    
    <div class="row col-md-9">
        {!!$advice->render()!!}
    </div>
@endsection

@section('otherJs')
@endsection