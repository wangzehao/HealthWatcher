@extends('app')
@section('otherResource')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <!--date-picker-->
<link href="{{asset('/common/lib/datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">



    <style>
        .sports-data {
            margin-top: 3em;
        }

        .subtitle {
            margin-top: 5px;;
        }

        .item1 {
            margin-top: 40px;
            color:white;
        }

        .item1-value{
            color: white;
            margin-top:0;
            padding-top:0;
        }

        .box {
            border: 1px solid #69D2E7;
            width: 150px;
            height: 150px;
            margin: 0 auto;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            -o-border-radius: 50%;
            border-radius: 50%;
            background-color: #69D2E7;;
        }

        .item{
            margin-top: 50px;
        }
    </style>
@endsection


@section('rightPanel')
            <div class="col-md-10">
                <div class="row">
                    <ul class="nav nav-tabs">
                        <li role="presentation"><a href="/health">健康数据</a></li>
                        <li role="presentation" class="active"><a href="/health/sports">我的运动</a></li>
                        <li role="presentation"><a href="/health/trend">最近趋势</a></li>

                    </ul>
                </div>
                <div class="row sports-data">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <div class="input-group date form_date " data-date="" data-date-format="dd MM yyyy"
                                 data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                <input class="form-control" size="16" type="text" value="{{Carbon\Carbon::today()->toDateString()}}" readonly>
                                <span class="input-group-addon"><span
                                        class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            <input type="hidden" id="dtp_input2" value=""/><br/>
                        </div>
                        <h3 class="subtitle text-primary">我的运动</h3>
                    </div>
                    <br />
                    <div class="jumbotron">
                        <div class="container">
                            <div class="col-md-3 box">
                                <h4 class="text-center item1">运动距离 ：</h4>
                                <h2 class="text-center  item1-value distance">{{$distance===''?"":"$distance"."km"}}</h2>
                            </div>

                            <div class="col-md-3 text-center text-primary  item">
                                <h4>运动时长：<span class="time">{{$time===""?"":"$time"."min"}}</span></h4>
                            </div>
                            <div class="col-md-3 text-center text-primary item">
                                <h4>燃烧热量：<span class="heat">{{$heat===""?"":"$heat"."kCal"}}</span></h4>
                            </div>
                            <div class="col-md-3 text-center text-primary item">
                                <h4>运动步数：<span class="steps">{{$steps===""?"":"$steps"."步"}}</span></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('otherJs')
    <script src="{{asset('/common/lib/datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>


<script>
    $('.form_date').datetimepicker({
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0,
        // format: 'yyyy-mm-dd',
        pickerPosition: "bottom-left"
    }).on('changeDate', function () {
        $.ajax({
            type: 'POST',
            url: '/health/ajaxSports',
            data: {date: $('#dtp_input2').val()},
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            success: function (data) {
                //alert(data.height);
                $(".distance").text(data.distance===""?"":data.distance+"km");
                $(".time").text(data.time===""?"":data.time+"min");
                $(".heat").text(data.heat===""?"":data.heat+"kCal");
                $(".steps").text(data.steps===""?"":data.steps+"步");
            },
            error: function (xhr, type) {
                alert('Ajax error!')
            }
        });
    });
</script>

@endsection