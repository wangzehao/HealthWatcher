@extends('app')
@section('otherResource')
    <meta name="_token" content="{{ csrf_token() }}"/>
        <!--date-picker-->
<link href="{{asset('/common/lib/datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">

<style>
        .health-data {
            margin-top: 3em;
        }

        .subtitle {
            margin-top: 5px;;
        }

        .item {
            margin-top: 40px;
            color: white;
        }

        .item-value {
            color: white;
            margin-top: 0;
            padding-top: 0;
        }

        .box {
            border: 1px solid #69D2E7;
            width: 150px;
            height: 150px;
            margin: 0 auto;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            -o-border-radius: 50%;
            border-radius: 50%;
            background-color: #69D2E7;
        }

        .box-2 {
            border: 1px solid #ff9966;
            width: 150px;
            height: 150px;
            margin: 0 auto;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            -o-border-radius: 50%;
            border-radius: 50%;
            background-color: #Ff9966;
        }

        .box-3 {
            border: 1px solid #ff9999;
            width: 150px;
            height: 150px;
            margin: 0 auto;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            -o-border-radius: 50%;
            border-radius: 50%;
            background-color: #ff9999;
        }

        .box-4 {
            border: 1px solid #99cc99;
            width: 150px;
            height: 150px;
            margin: 0 auto;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            -o-border-radius: 50%;
            border-radius: 50%;
            background-color: #99cc99;
        }
    </style>

@endsection


@section('rightPanel')
            <div class="col-md-10">
                <div class="row">
                    <ul class="nav nav-tabs">
                        <li role="presentation" class="active"><a href="/health">健康数据</a></li>
                        <li role="presentation"><a href="/health/sports">我的运动</a></li>
                        <li role="presentation"><a href="/health/trend">最近趋势</a></li>
                    </ul>
                </div>
                <div class="row health-data">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <div class="input-group date form_date " data-date="" data-date-format="dd MM yyyy"
                                 data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                <input class="form-control" size="16" type="text"
                                       value="{{Carbon\Carbon::today()->toDateString()}}" readonly>
                                <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            <input type="hidden" id="dtp_input2" value=""/><br/>
                        </div>
                        <h3 class="subtitle text-primary">健康数据</h3>
                    </div>
                    <br />
                    <div class="jumbotron">
                        <div class="container">
                            <div class="item-container col-md-3 ">
                                <div class="box">
                                    <h4 class="text-center item">身高 ：</h4>

                                    <h3 class="text-center  item-value height">{{$height==''?"":"$height"."cm"}}</h3>
                                </div>
                            </div>

                            <div class="item-container col-md-3 ">
                                <div class="box-2">
                                    <h4 class="text-center item">体重 ：</h4>

                                    <h3 class="text-center  item-value weight">{{$weight==''?"":"$weight"."kg"}}</h3>
                                </div>
                            </div>

                            <div class="item-container col-md-3 ">
                                <div class="box-3">
                                    <h4 class="text-center item">血压 ：</h4>

                                    <h3 class="text-center  item-value blood_pressure">{{$blood_pressure}}</h3>
                                </div>
                            </div>

                            <div class="item-container col-md-3 ">
                                <div class="box-4">
                                    <h4 class="text-center item">心率 ：</h4>

                                    <h3 class="text-center  item-value heart_rate">{{$heart_rate==""?"":"$heart_rate"."bpm"}}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


@endsection

@section('otherJs')

<script src="{{asset('/common/lib/datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>


<script>
    $('.form_date').datetimepicker({
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0,
       // format: 'yyyy-mm-dd',
        pickerPosition: "bottom-left"
    }).on('changeDate', function () {
        $.ajax({
            type: 'POST',
            url: '/health/ajax',
            data: {date: $('#dtp_input2').val()},
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            success: function (data) {
                //alert(data.height);
                $(".height").text(data.height==""?data.height:data.height+"cm");
                $(".weight").text(data.weight==""?data.weight:data.weight+"kg");
                $(".blood_pressure").text(data.blood_pressure);
                $(".heart_rate").text(data.heart_rate==""?data.heart_rate:data.heart_rate+"bpm");
            },
            error: function (xhr, type) {
                alert('Ajax error!')
            }
        });
    });
</script>
@endsection