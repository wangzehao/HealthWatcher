@extends('app')
@section('otherResource')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <!--date-picker-->
    <link href="{{asset('/common/lib/datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
    <script src="{{asset('/common/lib/chart/Chart.min.js')}}"></script>



    <style>
        .sports-data {
            margin-top: 3em;
        }

        .subtitle {
            margin-top: 5px;;
        }

        .item1 {
            margin-top: 40px;
            color:white;
        }

        .item1-value{
            color: white;
            margin-top:0;
            padding-top:0;
        }

        .box {
            border: 1px solid #69D2E7;
            width: 150px;
            height: 150px;
            margin: 0 auto;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            -o-border-radius: 50%;
            border-radius: 50%;
            background-color: #69D2E7;;
        }

        .item{
            margin-top: 50px;
        }
    </style>
@endsection


@section('rightPanel')
    <div class="col-md-10">
        <div class="row">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="/health">健康数据</a></li>
                <li role="presentation"><a href="/health/sports">我的运动</a></li>
                <li role="presentation" class="active"><a href="/health/trend">最近趋势</a></li>

            </ul>
        </div>
        <div class="row sports-data">
            <div class="row">
                <div class="form-group col-md-1">
                <!-- Single button -->
                <div class="btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        体重<span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="/health/trend/blood_pressure/7">血压</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/health/trend/sports/7">运动距离</a></li>
                    </ul>
                </div>
                    </div>

                <div class="form-group col-md-1">
                    <!-- Single button -->
                    <div class="btn-group">
                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            最近{{$days}}天变化情况 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="/health/trend/weight/7">7天</a></li>
                            <li><a href="/health/trend/weight/30">30天</a></li>
                            <li><a href="/health/trend/weight/60">60天</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <br />
            
                <canvas id="myChart" width="800" height="400"></canvas>
                <script>
                    var data = {
                       // labels : ["January","February","March","April","May","June","July"],
                        <?php
                            echo "labels:[";
                            foreach($health as $h)
                            {
                                echo "\"".$h['date']."\"".",";
                            }
                            echo "],"
                        ?>
                        // labels : ["January","February","March","April","May","June","July"],
                        datasets : [

                            {
                                fillColor : "rgba(151,187,205,0.5)",
                                strokeColor : "rgba(151,187,205,1)",
                                pointColor : "rgba(151,187,205,1)",
                                pointStrokeColor : "#fff",
                                <?php
                                    echo "data:[";
                                    foreach($health as $h)
                                    {
                                        echo (int)$h['weight'].",";
                                    }
                                    echo "]"
                                ?>
                             // data : [28,48,40,19,96,27,100]
                            }
                        ]
                    }
                    var ctx = document.getElementById("myChart").getContext("2d");
                    new Chart(ctx).Line(data);;
                </script>

            
        </div>
    </div>
    </div>

    </div>
    </div>

@endsection

@section('otherJs')

@endsection