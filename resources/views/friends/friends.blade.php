<?php use App\User?>
@extends('app') @section('otherResource') @endsection


@section('rightPanel')
<div class="row-fluid">
	<div class="col-md-3">
	<?php 
	foreach ($requests as $r){
		echo "<div>好友申请<h2>";
		echo User::findOrNew($r->me)->name;
		echo "</h2><br>";
		echo "<a href='/friends/accept?friendshipId=".$r->id."' class='btn btn-success'>接受</a>";
		echo " ";
		echo "<a href='/friends/refuse?friendshipId=".$r->id."' class='btn btn-danger'>拒绝</a>";
		echo "</div>";
		
	}
	?>
	<?php foreach($friends as $f){
		echo "<div><h2>";
		echo User::findOrNew($f->friend)->name." ";
		echo "<a class='btn btn-danger' href='/friends/remove?friendshipId=".$f->id."'>-</a></h2></div>";
	}?>
		<div>
		<form action="/friends/add">
		<input name="addFriendName" type="text" class="form-control" placeholder="好友名称"></input>
		<button type="submit" class="btn btn-primary">添加好友</button></form>
		
		</div>
	</div>
	<div class="col-md-9">
		<br> <a class="btn btn-primary" href="#publish"> 发布状态 </a>
		<?php foreach($news as $n){
		echo "<div><h3>".User::findOrNew($n->publisher)->name.":<br>".$n->updated_at."<br>".$n->title."</h3><br>".$n->content."<br></div>";
		}?>
		<form name="publish" action="/friend/publishNews">
			<input name="title" type="text" class="form-control" placeholder="标题"></input><br>
			<textarea name="content" class="form-control" placeholder="内容" rows="10"></textarea><br>
			<button type="submit" class="btn btn-primary" >发布</button>
		</form>
	</div>
</div>

@endsection @section('otherJs') @endsection
