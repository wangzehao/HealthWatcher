<?php
use App\Http\Controllers\InterestGroupController;
use App\User;
?>
@extends('app')

@section('otherResource')
@endsection


@section('rightPanel')
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="row-fluid">
			<form action="/interestgroup/group" method="get" class="form-search">
				<input name="groupName" class="input-medium search-query" type="text" /> <button type="submit" class="btn btn-primary">查找或创建</button>
				<a href="/interestgroup/explore" class="btn btn-success">发现</a>
			</form>
			
			</div>
			<div class="row-fluid">
			<h2>
			<?php 
			
			echo $groupName;
			$igc= new InterestGroupController();
			if($igc->inGroup($groupName)){
				echo "<a href='/interestgroup/leaveGroup?leaveGroupName=".$groupName."' class='btn btn-danger'>退出</a>";
			}else{
				echo "<a href='/interestgroup/joinGroup?joinGroupName=".$groupName."' class='btn btn-success'>加入</a>";
			}
			?>
			
			<a href="#publish" class="btn" >
				发表新贴
			 </a>
			 </h2>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<table class="table">
				<thead>
					<tr>
						<th>
							贴子
						</th>
						<th>
							发布者
						</th>
						<th>
							发布时间
						</th>
						<th>
							最后回复时间
						</th>
					</tr>
				</thead>
				<tbody>
				<?php 
					$colorCounter=0;
					foreach($posts as $p){
						echo "<tr";
						if($colorCounter%5==1){
							echo ' class="success"';
						}else if($colorCounter%5==2){
							echo ' class="danger"';
						}else if($colorCounter%5==3){
							echo ' class="warning"';
						}else if($colorCounter%5==4){
							echo ' class="info"';
						}
						echo ">";
						echo "<td>";
						echo "<a href='/interestgroup/post?postId=".$p->id."'>";
						echo $p->title;
						echo "</a>";
						echo "</td>";
						echo "<td>";
						echo User::findOrNew($p->publisher)->name;
						echo "</td>";
						echo "<td>";
						echo $p->created_at;
						echo "</td>";
						echo "<td>";
						echo $p->updated_at;
						echo "</td>";
						echo "</rd>";
						
						$colorCounter++;
					}
				
				?>
				
				</tbody>
			</table>
			{!!$posts->appends(['groupName'=>$groupName])->render()!!}
	</div>
	<div class="row-fluid">
		<form name="publish" action="/interestgroup/publishPost">
			<input id="reply" name="title" type="text" class="form-control" placeholder="标题"></input><br>
			<textarea name="content" class="form-control" placeholder="内容" rows="10"></textarea><br>
			<input name="groupName" type="hidden" value=<?php echo '"'.$groupName.'"'?>></input>
			<input name="fatherId" type="hidden" value=<?php echo '"0"'?>></input>
			
			<button type="submit" class="btn btn-primary" >发布</button>
		</form>
	</div>
</div>
@endsection

@section('otherJs')
@endsection