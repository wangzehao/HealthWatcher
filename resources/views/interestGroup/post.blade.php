<?php use App\User?>
@extends('app')

@section('otherResource')
@endsection


@section('rightPanel')
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="row-fluid">
			<form action="/interestgroup/group" method="get" class="form-search">
				<input name="groupName" class="input-medium search-query" type="text" /> <button type="submit" class="btn btn-primary">查找或创建</button>
				<a href="/interestgroup/explore" class="btn btn-success">发现</a>
			</form>			
			</div>
			<div class="row-fluid">
			<h2><?php echo "<a href='/interestgroup/group?groupName=".$post->group."'>".$post->group."</a> : ".$post->title?>
			<a class="btn" href="#publish">回复</a>
			 </h2>
			
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<table class="table">
				<thead>
					<tr>
						<th>
							<?php echo User::findOrNew($post->publisher)->name."：<br>".$post->content."<br><br>发布时间：".$post->created_at;?>
						</th>
					</tr>
				</thead>
				<tbody>
				<?php 
				$colorCounter=0;
				foreach ($children as $child){
				echo "<tr";
				if($colorCounter%5==1){
					echo ' class="success"';
				}else if($colorCounter%5==2){
					echo ' class="danger"';
				}else if($colorCounter%5==3){
					echo ' class="warning"';
				}else if($colorCounter%5==4){
					echo ' class="info"';
				}
				echo "><td><b>".User::findOrNew($child->publisher)->name."：<Br>".$child->title."</b><br>".$child->content."<br><br>发布时间：".$child->created_at;
				echo '<a class="btn" href="#publish" onclick="document.getElementById(\'reply\').value=\'回复：'.User::findOrNew($child->publisher)->name.'\'">回复</a></tr></td>';
				$colorCounter++;
				}?>
				</tbody>
			</table>
			{!!$children->setPath('/interestgroup/post')->appends(['postId'=>$post->id])->render()!!}
			
	</div>
	<div class="row-fluid">
		<form name="publish" action="/interestgroup/publishPost">
			<input id="reply" name="title" type="text" class="form-control" placeholder="标题"></input><br>
			<textarea name="content" class="form-control" placeholder="内容" rows="10"></textarea><br>
			<input name="groupName" type="hidden" value=<?php echo '"'.$post->group.'"'?>></input>
			<input name="fatherId" type="hidden" value=<?php echo '"'.$post->id.'"'?>></input>
			
			<button type="submit" class="btn btn-primary" >回复</button>
		</form>
	</div>
</div>
<br>
@endsection

@section('otherJs')
@endsection