<?php 
use App\Http\Controllers\InterestGroupController;
?>
@extends('app')

@section('otherResource')
@endsection


@section('rightPanel')
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<form action="/interestgroup/group" method="get" class="form-search">
			<div class="input-append">
				<input name="groupName" class="span2 search-query" type="text" /> <button type="submit" class="btn btn-primary">查找或创建</button>
			</div></form>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<?php
				$count=0;
				foreach ($groups as $group){
					if($count%4==0){
						echo '<div class="row-fluid">';
					}
					$groupName=$group->name;
					echo '<div class="col-xs-3 span3"><h2><a href="/interestgroup/group?groupName='.$groupName.'">'.$groupName.'</a></h2><br>';
					$igc= new InterestGroupController();
					if($igc->inGroup($group->name)){
						echo "<a href='/interestgroup/leaveGroup?leaveGroupName=".$groupName."' class='btn btn-danger'>退出</a>";
					}else{
						echo "<a href='/interestgroup/joinGroup?joinGroupName=".$groupName."' class='btn btn-success'>加入</a>";
					}
					echo '</div>';
					if($count%4==3){
						echo '</div>';
					}
					$count++;
				}
				if($count%4!=3){
					echo '</div>';
				}
				
			?>
			{!!$groups->render()!!}
		</div>
	</div>
	<br>
</div>
@endsection

@section('otherJs')
@endsection