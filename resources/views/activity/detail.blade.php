@extends('app')

@section('otherResource')
    <style>
        .activity-detail{
            font-size: 1.2em;
        }
    </style>
@endsection


@section('rightPanel')
    <div class="row">
        <h1 class="page-header">活动查看</h1>
    </div>

    <div class="row col-md-9">
    <h3 class="activity-title ">{{$activity['title']}}</h3>
    <hr/>
    <p class="activity-detail">{!! nl2br($activity['description']) !!}</p>
    </div>
@endsection

@section('otherJs')
@endsection