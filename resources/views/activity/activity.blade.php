@extends('app')

@section('otherResource')
    <style>
        a:hover {text-decoration: none;}
    </style>
@endsection


@section('rightPanel')
    <div class="row">
        <h1 class="page-header">活动查看</h1>
    </div>


    @foreach($activities as $activity)
    <div class="row col-md-9">
        <h4  ><a href="/activity/{{$activity['id']}}" >{{$activity['title']}}</a></h4>
        <p><?php
                $d=$activity['description'];
            if(mb_strlen($d,"utf-8")<200){
                echo $d;
            }else{
                echo mb_substr($d,0,200,'utf-8');
            }
            ?></p>
        <hr/>
    </div>
    @endforeach
    <div class="row col-md-9">
            {!!$activities->render()!!}
    </div>

@endsection

@section('otherJs')
@endsection