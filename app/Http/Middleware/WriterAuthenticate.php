<?php
/**
 * @author:ivan
 * Time: 2015/12/3 8:59
 */

namespace App\Http\Middleware;
use Closure;
use Illuminate\Contracts\Auth\Guard;

class WriterAuthenticate
{
    protected $loginPath='/login';
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($this->auth->check()){
            $userType=$this->auth->user()['type'];
            if($userType==2||$userType==3)
            {
                return $next($request);
            }
            return redirect($this->loginPath);
        }
        return redirect($this->loginPath);
    }

}