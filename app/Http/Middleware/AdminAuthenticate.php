<?php
/**
 * @author:ivan
 * Time: 2015/12/1 20:31
 */

namespace App\Http\Middleware;
use Closure;
use Illuminate\Contracts\Auth\Guard;

class AdminAuthenticate
{
    protected $loginPath='/login';
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	
        if($this->auth->check()){
        	//return "true";
            $userType=$this->auth->user()['type'];
            
            if($userType==1)
            {
            	//return $userType;
                return $next($request);
            }
            return redirect($this->loginPath);
        }
        return redirect($this->loginPath);
    }

}