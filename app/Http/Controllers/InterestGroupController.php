<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UserController;
use Illuminate\Database\QueryException;
use App\UserGroupRalation;
use Illuminate\Http\Request;
use App\Group;
use App\Post;
use DB;

class InterestGroupController extends Controller
{
	private function getCurrentUserId(){
		$uc = new UserController();
		return $uc->getCurrentUser()->id;
	}
	
	private function getCurrentUserName(){
		$uc = new UserController();
		return $uc->getCurrentUser()->name;
	}
	
	public function exploreGroups(Request $r){
		$groupsPerPage=12;
		$groups = Group::query()->paginate($groupsPerPage);
		return view('interestGroup.explore',['groups'=>$groups,'name'=>$this->getCurrentUserName()]);
	}
	
	public function createGroup(Request $r){
		$newGroup = Group::findOrNew($r->get('newGroupName'));
		$newGroup->name=$r->get('newGroupName');
		$newGroup->save();
		return $this->myVisitGroup($r->get('newGroupName'));
	}
	
	public function visitGroup(Request $r){
		$newGroup = Group::findOrNew($r->get('groupName'));
		$newGroup->name=$r->get('groupName');
		$newGroup->save();
		return $this->myVisitGroup($r->get('groupName'));
	}
	
	private function myVisitGroup(string $groupName){
		return view('interestGroup.group',['groupName'=>$groupName,'posts'=>$this->myGetGroupPosts($groupName),'name'=>$this->getCurrentUserName()]);
	}
	
	private function myGetGroupPosts(string $groupName){
		return Post::query()->where('group','=',$groupName)->where('father','=',0)->orderBy('updated_at','desc')->paginate(30);
	}
	
	private function myGetPostChildren(string $father){
		return Post::query()->where('father','=',$father)->paginate(30);
	}
	
	public function joinGroup(Request $r){
		$ugr = new UserGroupRalation();
		$ugr->group = $r->get('joinGroupName');
		$ugr->user=$this->getCurrentUserId();
		try{
			$ugr->save();
		}catch(QueryException $e){
		}
		return $this->myVisitGroup($r->get('joinGroupName'));
	}
	
	public function leaveGroup(Request $r){
		$group=$r->get('leaveGroupName');
		$user=$this->getCurrentUserId();
		DB::delete("delete from user_group_ralations where user='".$user."' and `group`='".$group."';");
		return $this->exploreGroups($r);
	}
	
	public function getJoinedGroups(){
		$user=$this->getCurrentUserId();
		$groups=UserGroupRalation::query()->where("user","=",$user)->lists('group');
		return $groups;
	}
	
	public function inGroup($group){
		$groups=$this->getJoinedGroups();
		return $groups->contains($group);
	}
	
	public function publishPost(Request $r){
		$post=new Post();
		$post->title=$r->get('title');
		$post->content=$r->get('content');
		$post->publisher=$this->getCurrentUserId();
		$post->group=$r->get('groupName');
		$post->father=$r->get('fatherId',0);
		$post->save();
		
		if($post->father==0){
			$children=Post::query()->where('father','=',$post->id)->paginate(30);
			return view('interestGroup.post',['post'=>$post,'children'=>$children,'name'=>$this->getCurrentUserName()]);
		}else{
			$father=Post::findOrNew($post->father);
			$father->touch();
			$children=Post::query()->where('father','=',$father->id)->paginate(30);
			return view('interestGroup.post',['post'=>$father,'children'=>$children,'name'=>$this->getCurrentUserName()]);
		}
		echo $post->title;
	}
	
	public function getPostDetails(Request $r){
		$post =Post::findOrNew($r->get('postId'));
		$children=Post::query()->where('father','=',$post->id)->paginate(30);
		return view('interestGroup.post',['post'=>$post,'children'=>$children,'name'=>$this->getCurrentUserName()]);
	}
}