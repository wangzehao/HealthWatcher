<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\UserController;
use Validator;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

/**
 * @author:ivan
 * Time: 2015/11/30 16:04
 */
class AdminAuthController extends Controller
{
    use AuthenticatesUsers,ThrottlesLogins;

    protected $redirectPath = '/admin/activityManagement';

    protected $username='name';

    protected  $loginPath='/admin';

    protected $toDoctorOrCoach='/adviceHome';

    protected $redirectAfterLogout='/admin'; //for method getLogout()

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest', ['except' => 'getLogout']);
    }

    public function index(){
        if(Auth::check()){
            $userType=Auth::user()['type'];
            if($userType==1){
                return redirect($this->redirectPath);
            }elseif($userType==2||$userType==3){
                return redirect($this->toDoctorOrCoach);
            }
        }
        return view('admin.login');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array $user
     * @return \Illuminate\Http\Response
     */
    protected function authenticated($request, $user)
    {
        switch ($user['type'])
        {
            case 1:
                return redirect($this->redirectPath);
            case 2||3:
                return redirect($this->toDoctorOrCoach);
            default:
                 return redirect($this->loginPath)
                     ->withInput($request->only($this->loginUsername(), 'remember'))
                    ->withErrors([
                        $this->loginUsername() => $this->getFailedLoginMessage(),
                     ]);
        }
    }
}