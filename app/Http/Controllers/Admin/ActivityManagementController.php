<?php
/**
 * @author:ivan
 * Time: 2015/11/30 16:43
 */

namespace App\Http\Controllers\Admin;


use App\Activity;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserController;
use Validator;
use Illuminate\Http\Request;
class ActivityManagementController extends Controller
{
    private $user;

    /**
     * ActivityManagementController constructor.
     */
    public function __construct()
    {
        $u=new UserController();
        $this->user=$u->getCurrentUser()['name'];
    }


    public function index(){
        $activities=Activity::orderBy('created_at','desc')->paginate(12);
        return view("admin.activityManagement.activity")->with(['name'=>$this->user,'activities'=>$activities,"i"=>1]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.activityManagement.create")->with('name',$this->user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
        $validator = $this->validator($data);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->getMessageBag());
        }

        Activity::create($data);
        return redirect('/admin/activityManagement')->with('successMsg','An activity is successfully created');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|max:255',
           'description'=>'required'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $activities=Activity::orderBy('created_at','desc')->paginate(5);

        return view('activity.activity')->with(['name'=>$this->user,'activities'=>$activities]);
    }

    /**
     * Show the detail of the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $activity=Activity::find($id);
        if($activity==null){
            abort(404);
        }
        return view('activity.detail')->with(['name'=>$this->user,'activity'=>$activity]);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activity=Activity::find($id);
        if($activity==null){
            abort(404);
        }
        return view('admin.activityManagement.update')->with(['name'=>$this->user,'activity'=>$activity]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $activity=Activity::find($id);
        if($activity==null){
            abort(404);
        }
        $data=$request->all();
        $validator = $this->validator($data);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->getMessageBag());
        }
        $activity->update($data);
        return redirect('/admin/activityManagement')->with('successMsg','An activity is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activity=Activity::find($id);
        if($activity==null){
            abort(404);
        }
       $activity->delete();
        return redirect()->back();
    }

}