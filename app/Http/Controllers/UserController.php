<?php

namespace App\Http\Controllers;

use App\Adviser;
use App\User;
use Illuminate\Support\Facades\Auth;
use  App\Http\Controllers\Controller;

class UserController extends Controller
{

    /**
     *@return array whose key is the field in table user;
     */
    public function getCurrentUser(){
        if(!Auth::check()){
            return null;
        }
        return Auth::user();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('health.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  array $data
     * @return User
     */
    public function store(array $data)
    {
        if((int)$data['type']==2||(int)$data['type']==3)
        {
            Adviser::create([
                'name'=>$data['name'],
                'type'=>(int)$data['type'],
                'countUsers'=>0,
            ]);
        }
        return User::create([
            'name' => $data['name'],
            'password' => bcrypt($data['password']),
            'type'=>(int)$data['type'],
        ]);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $name
     * @return array
     */
    public function find($name){
        return User::where('name',$name)->get();
    }

}