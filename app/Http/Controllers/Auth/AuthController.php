<?php

namespace App\Http\Controllers\Auth;


use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */


    use AuthenticatesUsers,ThrottlesLogins;

    protected $username='name';

    protected  $loginPath='/login';

    protected $redirectPath = '/health';

    private $toDoctorOrCoach='/adviceHome';

    private $redirectAdminPath = '/admin/activityManagement';

    protected $redirectAfterLogout='/login'; //for method getLogout()





    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array $user
     * @return \Illuminate\Http\Response
     */
    protected function authenticated($request, $user)
    {
    	//return Auth::user();
    	//return $user['type'];
        switch ($user['type'])
        {
        	
            case 0:
                return redirect($this->redirectPath);
            case 1:
                return redirect($this->redirectAdminPath);
            case 2||3:
                return redirect($this->toDoctorOrCoach);
            default:
                return redirect($this->loginPath)
                    ->withInput($request->only($this->loginUsername(), 'remember'))
                    ->withErrors([
                        $this->loginUsername() => $this->getFailedLoginMessage(),
                    ]);
        }
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        if(Auth::check()){
            $userType=Auth::user()['type'];
            if($userType==1){
                return redirect($this->redirectAdminPath);
            }elseif($userType==2||$userType===3){
                return redirect($this->toDoctorOrCoach);
            }else if($userType==0){
                return redirect($this->redirectPath);
            }
        }

        return view('login');
    }

}
