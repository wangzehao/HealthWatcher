<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function(){
    return redirect('/login');
});

// Registration routes...
Route::get('/register', 'Auth\RegisterController@getRegister');
Route::post('/register', 'Auth\RegisterController@postRegister');
Route::post('/register/ajax', 'Auth\RegisterController@checkUsername');








//Route::get("/admin",'Admin\AdminAuthController@index');
//Route::post("/admin",'Admin\AdminAuthController@postLogin');

// Admin
Route::group(['middleware'=>'adminAuth'],function(){
    Route::get('/admin/activityManagement','Admin\ActivityManagementController@index');
    Route::get('/admin/activityManagement/create','Admin\ActivityManagementController@create');
    Route::get('/admin/activityManagement/delete/{id}','Admin\ActivityManagementController@destroy');
    Route::get('/admin/activityManagement/update/{id}','Admin\ActivityManagementController@edit');
    Route::post('/admin/activityManagement/update/{id}','Admin\ActivityManagementController@update');
    Route::post('/admin/activityManagement/create','Admin\ActivityManagementController@store');
    Route::get('/admin/authorization','Admin\AuthorizationController@index');
    Route::post('/admin/authorization','Admin\AuthorizationController@store');
    
    //AutoAdvice
    Route::get('/autoAdvice','AutoAdviceController@management');
    Route::get('/autoAdvice/add','AutoAdviceController@addRule');
    Route::get('/autoAdvice/delete','AutoAdviceController@deleteRule');

});


Route::get('/adviceHome', function () {
    return view('welcome');
});

//user login
Route::get('/login','Auth\AuthController@getLogin');
Route::post('/login','Auth\AuthController@postLogin');

Route::get('logout','Auth\AuthController@getLogout');

Route::group(['middleware'=>'userAuth'],function(){
    Route::get('/health','Health\HealthController@index');
    Route::get('/health/trend','Health\HealthController@showTrend');
    Route::get('/health/trend/weight/{days}','Health\HealthController@showWeightTrend');
    Route::get('/health/trend/blood_pressure/{days}','Health\HealthController@showBloodPressureTrend');
    Route::get('/health/trend/sports/{days}','Health\HealthController@showSportsTrend');
    Route::get('/health/sports','Health\HealthController@showSports');
    Route::post('/health/ajax','Health\HealthController@getDataAjax');
    Route::post('/health/ajaxSports','Health\HealthController@getSportsDataAjax');
    Route::get('/activity','Admin\ActivityManagementController@show');
    Route::get('/activity/{id}','Admin\ActivityManagementController@detail');
    Route::get('/advice','Advice\AdviceController@index');

    //InterestGroup
    Route::get('/interestgroup/explore','InterestGroupController@exploreGroups');
    Route::get('/interestgroup/createGroup','InterestGroupController@createGroup');
    Route::get('/interestgroup/group','InterestGroupController@visitGroup');
    Route::get('/interestgroup/joinGroup','InterestGroupController@joinGroup');
    Route::get('/interestgroup/leaveGroup','InterestGroupController@leaveGroup');
    Route::get('/interestgroup/getJoinedGroups','InterestGroupController@getJoinedGroups');
    Route::get('/interestgroup/post','InterestGroupController@getPostDetails');
    Route::get('/interestgroup/publishPost','InterestGroupController@publishPost');
    //Friends
    Route::get('/friends','FriendsController@homepage');
    Route::get('/friends/add','FriendsController@addFriend');
    Route::get('/friends/accept','FriendsController@acceptFriendshipReqest');
    Route::get('/friends/refuse','FriendsController@refuseFriendshipRequest');
    Route::get('/friends/remove','FriendsController@removeFriend');
    Route::get('/friend/publishNews','FriendsController@publishNews');
});


Route::group(['middleware'=>'writerAuth'],function(){
    Route::get('/adviceHome','Advice\AdviceController@showAdmin');
    Route::get('/adviceHome/userHealthData/{u}','Advice\AdviceController@getUserHealthData');
    Route::get('/adviceHome/userSportsData/{u}','Advice\AdviceController@getUserSportsData');
    Route::get('/adviceHome/adviceTo/{u}','Advice\AdviceController@create');
    Route::post('/adviceHome/adviceTo/{u}','Advice\AdviceController@store');
    Route::get('/adviceHome/upload','Advice\AdviceController@showUpload');
    Route::post('/adviceHome/upload','Advice\AdviceController@upload');
});

