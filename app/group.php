<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model{
	protected $primaryKey='name';
	public $timestamps=false;
	protected $fillable = ['name'];
}