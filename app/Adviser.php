<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adviser extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'advisers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','type','countUsers'];
}
