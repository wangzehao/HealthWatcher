<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdviserUserRelation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'adviser_user_relation';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['adviser','user'];
}
