<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroupRalation extends Model
{
	public $timestamps=false;
	protected $fillable = ['user','group'];
}
