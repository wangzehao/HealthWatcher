<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friendship extends Model
{
    protected $fillable = ['me','friend','agreed'];
    public $timestamps=false;
}
