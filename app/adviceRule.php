<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdviceRule extends Model
{
    protected $fillable = ['title', 'describe','rule','content'];
}
