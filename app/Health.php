<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Health extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'health';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user','height','weight','blood_pressure','heart_rate','date'];
}
