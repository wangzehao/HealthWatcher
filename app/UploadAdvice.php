<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadAdvice extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'uploadAdvice';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['adviser','content'];
}
